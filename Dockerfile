FROM python:3-alpine

COPY [ "queue-runner.py", "requirements.txt", "startup.sh", "/opt/queue-runner/" ]

RUN pip install --no-cache-dir -r /opt/queue-runner/requirements.txt && \
    chmod 755 /opt/queue-runner/queue-runner.py && \
    chmod 755 /opt/queue-runner/startup.sh

CMD [ "/opt/queue-runner/queue-runner.py" ]
