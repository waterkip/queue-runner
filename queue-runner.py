#! /usr/local/bin/python3 -u
"""
Simple Python-based queue item processor.

It connects to RabbitMQ, and listens for all messages with a routing key
matching "zs.v0.#" on the configured exchange (default: "amq.topic")
"""

import argparse
import json
import logging
import pika
import requests
import sys

from datetime import datetime
from pythonjsonlogger import jsonlogger
from statsd import StatsClient

class CustomJsonFormatter(jsonlogger.JsonFormatter):
    """
    Custom JSON log formatter, that adds "zs_component" and "timestamp" fields
    """

    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)

        log_record['zs_component'] = 'queue-runner'

        created = datetime.fromtimestamp(record.created).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        log_record['timestamp'] = created

        if log_record.get('level'):
            log_record['level'] = log_record['level'].upper()
        else:
            log_record['level'] = record.levelname

logger = logging.getLogger()

logHandler = logging.StreamHandler()
formatter = CustomJsonFormatter('(timestamp) (level) (zs_component) (message)')
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)

def get_message_handler(statsd):
    def handle_message(channel, method, properties, body):
        """
        Handle an incoming message from RabbitMQ (by HTTP POSTing it)
        """
        logger.info("Received message, routing key: '%s'" % method.routing_key)
        statsd.incr(method.routing_key)

        try:
            message = json.loads(body)
        except JSONDecodeError as e:
            logger.error("Could not parse message: %s" % e)
            channel.basic_reject(method.delivery_tag, requeue=False)
            statsd.incr('corrupt_item')
            return

        if 'url' not in message:
            logger.warn("No dispatch URL found in message.")
            channel.basic_reject(method.delivery_tag, requeue=False)
            statsd.incr('corrupt_item.no_url')
            return

        url = message['url']
        del message['url']

        logger.info("Dispatching message to '%s'" % (url), extra={ "queue_url": url })

        with statsd.timer('item.%s.time' % method.routing_key):
            r = requests.post(url, json=message, verify=False)

        try:
            parsed_response = json.loads(r.text)
            json_response = parsed_response['result'][0]
        except:
            json_response = None

        request_id = 'unknown'

        if 'zs-req-id' in r.headers:
            request_id = r.headers['zs-req-id']

        if (r.status_code != requests.codes.ok):
            base = "Error while running action (%s: %s)" % (r.status_code, r.reason)

            if json_response:
                log_message = '%s: exception "%s": %s' % (base, json_response['type'], ', '.join(json_response['messages']))
            else:
                log_message = '%s: "%s"' % (base, r.text)

            logger.warn(log_message, extra={ "queue_url": url, "request_id": request_id })

            channel.basic_reject(method.delivery_tag, requeue=False)
            return

        # Everything went well!
        logger.info("Queue item handled successfully.",
            extra={ "queue_url": url, "request_id": request_id })
        channel.basic_ack(method.delivery_tag)

        if json_response:
            statsd.incr('item.%s.%s' % (method.routing_key, json_response.get('status', 'unknown')))
        else:
            statsd.incr('corrupt_item')
            logger.warn("Corrupt response body: '%s'" % r.text);

        return

    return handle_message

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--rabbitmq-url", help="amqp:// URL used to contact RabbitMQ server")
    parser.add_argument("--statsd-host", default="localhost", help="Hostname/IP of the StatsD server")
    parser.add_argument("--statsd-port", default="8125", help="Port the StatsD server can be reached on")
    parser.add_argument("--exchange", default='amq.topic', help="AMQP exchange to use")
    parser.add_argument("--queue", default='zaaksysteem-queue-runner', help="Queue name to use")
    parser.add_argument("--debug", action="store_true", help="Run in debug mode, increasing verbosity")

    args = parser.parse_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logger.info("Queue processor starting up")

    connection = pika.BlockingConnection(pika.URLParameters(args.rabbitmq_url))

    statsd = StatsClient(args.statsd_host, args.statsd_port, prefix= 'zaaksysteem.queue.statsd')

    queue_message_handler = get_message_handler(statsd)

    channel = connection.channel()
    channel.queue_declare(queue=args.queue, durable=True)
    channel.queue_bind(exchange=args.exchange, queue=args.queue, routing_key='zs.v0.#')
    channel.basic_consume(queue_message_handler, queue=args.queue, no_ack=False)
    channel.start_consuming()
