#!/bin/sh

MQ_HOST="$1"
MQ_PORT="$2"
MQ_URL="$3"
RUNNER_ARGS="$4"

while ! nc -z "$MQ_HOST" "$MQ_PORT"; do
    echo "Waiting for RabbitMQ"
    sleep 1
done

/opt/queue-runner/queue-runner.py --rabbitmq-url="$MQ_URL" "$RUNNER_ARGS"
